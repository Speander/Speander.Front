import { AutoMap } from '@nartc/automapper';

export class EventLocationResponse {
    @AutoMap()
    public id: string;

    constructor() {
        this.id = '';
    }
}
