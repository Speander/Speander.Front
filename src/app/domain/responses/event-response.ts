import { AutoMap } from '@nartc/automapper';

export class EventResponse {
    @AutoMap()
    public id: string;
    @AutoMap()
    public name: string;
    @AutoMap()
    public desription: string;

    constructor() {
        this.id = '';
        this.name = '';
        this.desription = '';
    }
}
