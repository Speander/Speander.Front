import { AutoMap } from '@nartc/automapper';
import { LocationRequest } from '@speander/domain/requests/location-request';

export class EventLocationRequest {
    @AutoMap(() => LocationRequest)
    public location?: LocationRequest;
}
