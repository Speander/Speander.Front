import { AutoMap } from '@nartc/automapper';

export class EventTimeRequest {
    @AutoMap()
    public from: string;
    @AutoMap()
    public to: string;

    constructor() {
        this.from = '';
        this.to = '';
    }
}
