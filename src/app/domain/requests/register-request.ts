import { AutoMap } from '@nartc/automapper';

export class RegisterRequest {
    @AutoMap()
    public readonly email: string;
    @AutoMap()
    public readonly password: string;

    constructor() {
        this.email = '';
        this.password = '';
    }
}
