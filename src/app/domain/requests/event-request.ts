import { AutoMap } from '@nartc/automapper';

export class EventRequest {
    @AutoMap()
    public name: string;
    @AutoMap()
    public description: string;

    constructor() {
        this.name = '';
        this.description = '';
    }
}
