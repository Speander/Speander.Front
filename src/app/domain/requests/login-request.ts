import { AutoMap } from '@nartc/automapper';

export class LoginRequest {
    @AutoMap()
    public readonly email: string;
    @AutoMap()
    public readonly password: string;

    constructor() {
        this.email = '';
        this.password = '';
    }
}
