import { AutoMap } from '@nartc/automapper';

export class RegisterForm {
    @AutoMap()
    public email: string;
    @AutoMap()
    public password: string;
    @AutoMap()
    public confirmPassword: string;

    constructor() {
        this.email = '';
        this.password = '';
        this.confirmPassword = '';
    }
}
