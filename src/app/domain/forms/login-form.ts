import { AutoMap } from '@nartc/automapper';

export class LoginForm {
    @AutoMap()
    public email: string;
    @AutoMap()
    public password: string;

    constructor() {
        this.email = '';
        this.password = '';
    }
}
