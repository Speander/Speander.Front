import { AutoMap } from '@nartc/automapper';

export class EventLocationForm {
    @AutoMap()
    public country: string;
    @AutoMap()
    public city: string;
    @AutoMap()
    public street: string;
    @AutoMap()
    public building: string;
    @AutoMap()
    public longitude: string;
    @AutoMap()
    public latitude: string;
    @AutoMap()
    public comment: string;

    constructor() {
        this.building = '';
        this.city = '';
        this.comment = '';
        this.country = '';
        this.latitude = '';
        this.longitude = '';
        this.street = '';
    }
}
