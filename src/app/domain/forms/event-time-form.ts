import { AutoMap } from '@nartc/automapper';

export class EventTimeForm {
    @AutoMap()
    public from: Date;
    @AutoMap()
    public to: Date;

    constructor() {
        this.from = new Date();
        this.to = new Date();
    }
}
