import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './modules/core/core.module';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Profile } from './modules/core/auto-mapper/profile';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        CoreModule.injectCore({
            apiUrl: environment.apiUrl,
            production: environment.production,
            accessTokenKey: environment.accessTokenKey,
            endpoints: environment.endpoints,
            profiles: [
                Profile
            ],
            authorizationKey: environment.authorizationKey,
            authorizationMethod: environment.authorizationMethod
        }),
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
