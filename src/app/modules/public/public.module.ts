import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from '@speander/shared/shared.module';
import { RouterModule } from '@angular/router';


@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '',
                pathMatch: 'full',
                component: HomeComponent
            }
        ])
    ]
})
export class PublicModule {
}
