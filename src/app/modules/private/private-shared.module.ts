import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivateLayoutComponent } from './components/private-layout/private-layout.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@speander/shared/shared.module';


@NgModule({
    declarations: [PrivateLayoutComponent],
    imports: [
        CommonModule,
        RouterModule,
        SharedModule
    ],
    exports: [PrivateLayoutComponent]
})
export class PrivateSharedModule {
}
