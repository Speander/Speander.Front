import {
    Component,
    ViewChild
} from '@angular/core';
import {
    BreakpointObserver,
    Breakpoints
} from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SessionService } from '@speander/core/services/session.service';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
    selector: 'sp-private-layout',
    templateUrl: './private-layout.component.html',
    styleUrls: ['./private-layout.component.scss']
})
export class PrivateLayoutComponent {
    @ViewChild(MatSidenav) private matSidenav?: MatSidenav;

    public get isHandset(): Observable<boolean> {
        return this.breakpointObserver.observe(Breakpoints.Handset).pipe(
            map(result => result.matches)
        );
    }

    constructor(
        private breakpointObserver: BreakpointObserver,
        private readonly session: SessionService,
        private readonly router: Router) {}

    public logout(): void {
        this.router.navigateByUrl('').then(() => {
            this.session.logout();
        });
    }

    public close(): void {
        if (this.matSidenav?.mode === 'over') {
            this.matSidenav?.close();
        }
    }
}
