import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PrivateLayoutComponent } from '@speander/private/components/private-layout/private-layout.component';
import { PrivateSharedModule } from '@speander/private/private-shared.module';


@NgModule({
    declarations: [DashboardComponent],
    imports: [
        CommonModule,
        PrivateSharedModule,
        RouterModule.forChild([
            {
                path: '',
                component: PrivateLayoutComponent,
                children: [
                    {
                        path: '',
                        component: DashboardComponent,
                        pathMatch: 'full'
                    }
                ]
            }
        ])
    ]
})
export class DashboardModule {
}
