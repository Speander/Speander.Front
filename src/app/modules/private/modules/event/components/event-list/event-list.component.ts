import {
    Component,
    OnInit
} from '@angular/core';
import { EventStore } from '@speander/private/modules/event/+state/event.store';
import { EventService } from '@speander/private/modules/event/services/event.service';
import { first } from 'rxjs/operators';
import { Event } from '@speander/domain/models/event';
import { EventQuery } from '@speander/private/modules/event/+state/event.query';

@Component({
    selector: 'sp-event-list',
    templateUrl: './event-list.component.html',
    styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

    constructor(
        private readonly eventStore: EventStore,
        private readonly eventService: EventService,
        public readonly eventQuery: EventQuery) { }

    ngOnInit(): void {
        this.eventService.getOwnedEvents().pipe(first()).subscribe(
            (events: Event[]) => {
                this.eventStore.add(events);
            }
        );
    }

}
