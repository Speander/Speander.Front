import {
    Component,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import { EventService } from '@speander/private/modules/event/services/event.service';
import { NgFormsManager } from '@ngneat/forms-manager';
import { IEventForms } from '@speander/private/modules/event/interfaces/event-forms';
import { Control } from '@ngneat/forms-manager/lib/types';
import { EventForm } from '@speander/domain/forms/event-form';
import { EventForms } from '@speander/private/modules/event/enums/event-forms';
import { Mapper } from '@nartc/automapper';
import { EventRequest } from '@speander/domain/requests/event-request';
import { first } from 'rxjs/operators';
import {
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { EventResponse } from '@speander/domain/responses/event-response';
import { EventStore } from '@speander/private/modules/event/+state/event.store';
import { Event } from '@speander/domain/models/event';
import { EventLocationForm } from '@speander/domain/forms/event-location-form';
import { EventLocationRequest } from '@speander/domain/requests/event-location-request';
import { EventLocationResponse } from '@speander/domain/responses/event-location-response';
import { EventTimeForm } from '@speander/domain/forms/event-time-form';
import { EventTimeRequest } from '@speander/domain/requests/event-time-request';
import { EventTimeResponse } from '@speander/domain/responses/event-time-response';
import { Router } from '@angular/router';

@Component({
    selector: 'sp-new-event',
    templateUrl: './new-event.component.html',
    styleUrls: ['./new-event.component.scss']
})
export class NewEventComponent implements OnInit, OnDestroy {
    @ViewChild(MatHorizontalStepper) private stepper?: MatHorizontalStepper;

    public readonly eventForm: FormGroup;
    public readonly eventLocationForm: FormGroup;
    public readonly eventTimeForm: FormGroup;

    private eventId?: string;
    private eventLocationId?: string;

    constructor(
        private readonly event: EventService,
        private readonly formsManager: NgFormsManager<IEventForms>,
        builder: FormBuilder,
        private readonly eventStore: EventStore,
        private readonly router: Router) {
        this.eventForm = builder.group({
            name: [
                null,
                Validators.required
            ],
            description: [
                null,
                Validators.required
            ]
        });
        this.eventLocationForm = builder.group({
            country: [
                null,
                Validators.required
            ],
            city: [
                null,
                Validators.required
            ],
            street: [
                null,
                Validators.required
            ],
            building: [
                null,
                Validators.required
            ],
            longitude: [
                null,
                Validators.required
            ],
            latitude: [
                null,
                Validators.required
            ],
            comment: [null]
        });
        this.eventTimeForm = builder.group({
            from: [
                null,
                Validators.required
            ],
            to: [
                null,
                Validators.required
            ]
        });
        formsManager.upsert(EventForms.EVENT, this.eventForm);
        formsManager.upsert(EventForms.EVENT_LOCATION, this.eventLocationForm);
        formsManager.upsert(EventForms.EVENT_TIME, this.eventTimeForm);
    }

    public ngOnInit(): void {
    }

    public createEvent(): void {
        const form: Control<EventForm> | null = this.formsManager.getControl(EventForms.EVENT);
        if (form?.valid) {
            this.event.createEvent(Mapper.map(form.value, EventRequest, EventForm)).pipe(first()).subscribe(
                (response: EventResponse) => {
                    this.eventId = response.id;
                    this.eventStore.add(Mapper.map(response, Event, EventResponse));
                    this.stepper?.next();
                }
            );
        }
    }

    public addLocation(): void {
        const form: Control<EventLocationForm> | null = this.formsManager.getControl(EventForms.EVENT_LOCATION);
        if (form?.valid && !!this.eventId) {
            this.event.addLocation(this.eventId, Mapper.map(form.value, EventLocationRequest, EventLocationForm)).pipe(
                first()
            ).subscribe(
                (response: EventLocationResponse) => {
                    this.eventLocationId = response.id;
                    this.stepper?.next();
                }
            );
        }
    }

    public addTime(): void {
        const form: Control<EventTimeForm> | null = this.formsManager.getControl(EventForms.EVENT_TIME);
        if (form?.valid && !!this.eventLocationId && !!this.eventId) {
            this.event.addTime(
                this.eventId,
                this.eventLocationId,
                [Mapper.map(form.value, EventTimeRequest, EventTimeForm)]).pipe(
                first()
            ).subscribe(
                (response: EventTimeResponse[]) => {
                    this.stepper?.next();
                }
            );
        }
    }

    public confirmEvent(): void {
        if (!!this.eventId && this.eventForm.valid && this.eventLocationForm.valid && this.eventTimeForm.valid) {
            this.event.confirmEvent(this.eventId).pipe(first()).subscribe(
                () => {
                    this.router.navigateByUrl('/events');
                }
            );
        }
    }

    public ngOnDestroy(): void {
        this.formsManager.unsubscribe(EventForms.EVENT);
        this.formsManager.unsubscribe(EventForms.EVENT_LOCATION);
        this.formsManager.unsubscribe(EventForms.EVENT_TIME);
    }
}
