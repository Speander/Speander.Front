export enum EventForms {
    EVENT = 'event',
    EVENT_LOCATION = 'eventLocation',
    EVENT_TIME = 'eventTime'
}
