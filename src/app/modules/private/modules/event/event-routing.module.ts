import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivateLayoutComponent } from '@speander/private/components/private-layout/private-layout.component';
import { EventListComponent } from '@speander/private/modules/event/components/event-list/event-list.component';
import { NewEventComponent } from '@speander/private/modules/event/components/new-event/new-event.component';


const routes: Routes = [
    {
        path: '',
        component: PrivateLayoutComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: EventListComponent
            },
            {
                path: 'new',
                component: NewEventComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule { }
