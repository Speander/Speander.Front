import { EventForms } from '@speander/private/modules/event/enums/event-forms';
import { EventForm } from '@speander/domain/forms/event-form';
import { EventLocationForm } from '@speander/domain/forms/event-location-form';
import { EventTimeForm } from '@speander/domain/forms/event-time-form';

export interface IEventForms {
    [EventForms.EVENT]: EventForm;
    [EventForms.EVENT_LOCATION]: EventLocationForm;
    [EventForms.EVENT_TIME]: EventTimeForm;
}
