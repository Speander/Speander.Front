import {
    EntityState,
    EntityStore,
    StoreConfig
} from '@datorama/akita';
import { Event } from '@speander/domain/models/event';
import { Injectable } from '@angular/core';

export interface EventState extends EntityState<Event, string> {
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'event', idKey: 'id' })
export class EventStore extends EntityStore<EventState> {
    constructor() {
        super();
    }
}
