import { QueryEntity } from '@datorama/akita';
import {
    EventState,
    EventStore
} from '@speander/private/modules/event/+state/event.store';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class EventQuery extends QueryEntity<EventState> {
    constructor($store: EventStore) {
        super($store);
    }
}
