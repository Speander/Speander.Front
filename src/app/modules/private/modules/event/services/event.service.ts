import {
    Inject,
    Injectable
} from '@angular/core';
import { HttpService } from '@speander/core/services/http.service';
import { EventRequest } from '@speander/domain/requests/event-request';
import { Observable } from 'rxjs';
import { EventResponse } from '@speander/domain/responses/event-response';
import { ENDPOINTS } from '@speander/core/injection-tokens/endpoints';
import { IReadonlyDictionary } from '@speander/core/interfaces/dictionary';
import { Endpoint } from '@speander/core/enums/endpoint';
import { EventLocationRequest } from '@speander/domain/requests/event-location-request';
import { EventLocationResponse } from '@speander/domain/responses/event-location-response';
import { EventTimeRequest } from '@speander/domain/requests/event-time-request';
import { EventTimeResponse } from '@speander/domain/responses/event-time-response';
import { EventConfirmedResponse } from '@speander/domain/responses/event-confirmed-response';
import { Event } from '@speander/domain/models/event';

@Injectable()
export class EventService {

    constructor(
        private readonly http: HttpService,
        @Inject(ENDPOINTS) private readonly endpoints: IReadonlyDictionary<string>) { }

    public createEvent(event: EventRequest): Observable<EventResponse> {
        return this.http.post(this.endpoints[Endpoint.CREATE_EVENT], event);
    }

    public addLocation(eventId: string, location: EventLocationRequest): Observable<EventLocationResponse> {
        return this.http.post(this.endpoints[Endpoint.ADD_LOCATION].replace('{eventId}', eventId), location);
    }

    public addTime(
        eventId: string,
        eventLocationId: string,
        eventTime: EventTimeRequest[]): Observable<EventTimeResponse[]> {
        return this.http.post(
            this.endpoints[Endpoint.ADD_TIME].replace('{eventId}', eventId)
                                             .replace('{eventLocationId}', eventLocationId),
            eventTime);
    }

    public confirmEvent(eventId: string): Observable<EventConfirmedResponse> {
        return this.http.patch(this.endpoints[Endpoint.CONFIRM_EVENT].replace('{eventId}', eventId), null);
    }

    public getOwnedEvents(): Observable<Event[]> {
        return this.http.get(this.endpoints[Endpoint.OWNED_EVENTS]);
    }
}
