import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventRoutingModule } from './event-routing.module';
import { EventListComponent } from './components/event-list/event-list.component';
import { NewEventComponent } from './components/new-event/new-event.component';
import { EventService } from '@speander/private/modules/event/services/event.service';
import { SharedModule } from '@speander/shared/shared.module';
import { PrivateSharedModule } from '@speander/private/private-shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';


@NgModule({
    declarations: [
        EventListComponent,
        NewEventComponent
    ],
    imports: [
        CommonModule,
        EventRoutingModule,
        SharedModule,
        PrivateSharedModule,
        ReactiveFormsModule
    ],
    providers: [
        EventService,
        {
            provide: STEPPER_GLOBAL_OPTIONS,
            useValue: { showError: true }
        }
    ]
})
export class EventModule {
}
