export enum Endpoint {
    LOGIN = 'login',
    REGISTER = 'register',
    CREATE_EVENT = 'event',
    CONFIRM_EVENT = 'confirmEvent',
    ADD_LOCATION = 'addLocation',
    ADD_TIME = 'addTime',
    OWNED_EVENTS = 'ownedEvents'
}
