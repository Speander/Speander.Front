import {
    APP_INITIALIZER,
    ModuleWithProviders,
    NgModule
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    HTTP_INTERCEPTORS,
    HttpClientModule
} from '@angular/common/http';
import { API_URL } from './injection-tokens/api-url';
import { ICoreOptions } from './interfaces/core-options';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ACCESS_TOKEN_KEY } from './injection-tokens/access-token-key';
import { IS_PRODUCTION } from './injection-tokens/is-production';
import { ENDPOINTS } from './injection-tokens/endpoints';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { SessionService } from './services/session.service';
import { AUTHORIZATION_KEY } from './injection-tokens/authorization-key';
import { AUTHORIZATION_METHOD } from './injection-tokens/authorization-method';
import { MAPPING_PROFILES } from './injection-tokens/mapping-profiles';
import { initializeMapper } from './auto-mapper/initialize-mappper';
import { retrieveToken } from '@speander/core/functions/retrieve-token';


@NgModule({
    declarations: [NotFoundComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        NgxWebstorageModule.forRoot({
            prefix: 'sp',
            separator: '.',
            caseSensitive: true
        })
    ],
    exports: [
        NotFoundComponent
    ]
})
export class CoreModule {
    public static injectCore(options: ICoreOptions): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                {
                    provide: API_URL,
                    useValue: options.apiUrl
                },
                {
                    provide: ACCESS_TOKEN_KEY,
                    useValue: options.accessTokenKey
                },
                {
                    provide: IS_PRODUCTION,
                    useValue: options.production
                },
                {
                    provide: ENDPOINTS,
                    useValue: options.endpoints
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true,
                    deps: [
                        SessionService,
                        AUTHORIZATION_KEY,
                        AUTHORIZATION_METHOD
                    ]
                },
                {
                    provide: MAPPING_PROFILES,
                    useValue: options.profiles
                },
                {
                    provide: APP_INITIALIZER,
                    useFactory: initializeMapper,
                    deps: [MAPPING_PROFILES],
                    multi: true
                },
                {
                    provide: AUTHORIZATION_KEY,
                    useValue: options.authorizationKey
                },
                {
                    provide: AUTHORIZATION_METHOD,
                    useValue: options.authorizationMethod
                },
                {
                    provide: APP_INITIALIZER,
                    useFactory: retrieveToken,
                    deps: [SessionService],
                    multi: true
                }
            ]
        };
    }
}
