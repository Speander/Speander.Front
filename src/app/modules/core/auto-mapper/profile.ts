import {
    AutoMapper,
    MappingProfileBase
} from '@nartc/automapper';
import { LoginForm } from '@speander/domain/forms/login-form';
import { LoginRequest } from '@speander/domain/requests/login-request';
import { RegisterForm } from '@speander/domain/forms/register-form';
import { RegisterRequest } from '@speander/domain/requests/register-request';
import { EventForm } from '@speander/domain/forms/event-form';
import { EventRequest } from '@speander/domain/requests/event-request';
import { EventResponse } from '@speander/domain/responses/event-response';
import { Event } from '@speander/domain/models/event';
import { EventLocationForm } from '@speander/domain/forms/event-location-form';
import { EventLocationRequest } from '@speander/domain/requests/event-location-request';
import { Converter } from '@nartc/automapper/dist/types';
import { EventTimeForm } from '@speander/domain/forms/event-time-form';
import { EventTimeRequest } from '@speander/domain/requests/event-time-request';

export class Profile extends MappingProfileBase {
    constructor(mapper: AutoMapper) {
        super();
        mapper.createMap(LoginForm, LoginRequest);
        mapper.createMap(RegisterForm, RegisterRequest)
              .forMember(x => x.email, opts => opts.mapFrom(x => x.email))
              .forMember(x => x.password, opts => opts.mapFrom(x => x.password));
        mapper.createMap(EventForm, EventRequest);
        mapper.createMap(EventResponse, Event);
        mapper.createMap(EventLocationForm, EventLocationRequest)
              .forMember(x => x.location, opts => opts.mapFrom(x => x));
        mapper.createMap(EventTimeForm, EventTimeRequest)
              .forMember(x => x.from, opts => opts.convertUsing(new DateConverter(), x => x.from))
              .forMember(x => x.to, opts => opts.convertUsing(new DateConverter(), x => x.to));
    }
}

export class DateConverter implements Converter<Date, string> {
    public convert(source: Date): string {
        return source.toISOString().split('.')[0];
    }
}
