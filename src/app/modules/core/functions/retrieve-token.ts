import { SessionService } from '@speander/core/services/session.service';

export function retrieveToken(sessionService: SessionService) {
    return () => sessionService.retrieveToken();
}
