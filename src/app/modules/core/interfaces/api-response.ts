export interface IApiResponse<TResponse> {
    data: TResponse;
    errors?: any;
}
