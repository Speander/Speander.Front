import { IReadonlyDictionary } from './dictionary';
import {
    AutoMapper,
    MappingProfile
} from '@nartc/automapper';

export interface ICoreOptions {
    production: boolean;
    apiUrl: string;
    accessTokenKey: string;
    endpoints: IReadonlyDictionary<string>;
    profiles: (new (mapper: AutoMapper) => MappingProfile)[];
    authorizationKey: string;
    authorizationMethod: string;
}
