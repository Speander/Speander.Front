import { IReadonlyDictionary } from './dictionary';

export interface IEnvironment {
    production: boolean;
    apiUrl: string;
    accessTokenKey: string;
    endpoints: IReadonlyDictionary<string>;
    authorizationKey: string;
    authorizationMethod: string;
}
