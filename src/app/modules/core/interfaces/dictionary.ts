export interface IReadonlyDictionary<T> {
    readonly [key: string]: T;
}
