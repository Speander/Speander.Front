import { InjectionToken } from '@angular/core';
import { IReadonlyDictionary } from '../interfaces/dictionary';

export const ENDPOINTS: InjectionToken<IReadonlyDictionary<string>> = new InjectionToken<IReadonlyDictionary<string>>(
    'ENDPOINTS');
