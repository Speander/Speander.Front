import {
    AutoMapper,
    MappingProfile
} from '@nartc/automapper';
import { InjectionToken } from '@angular/core';

export const MAPPING_PROFILES: InjectionToken<(new (mapper: AutoMapper) => MappingProfile)[]> =
    new InjectionToken<(new (mapper: AutoMapper) => MappingProfile)[]>('MAPPING_PROFILES');
