import {
    Inject,
    Injectable
} from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpHeaders,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';
import { AUTHORIZATION_KEY } from '../injection-tokens/authorization-key';
import { AUTHORIZATION_METHOD } from '../injection-tokens/authorization-method';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private readonly session: SessionService,
        @Inject(AUTHORIZATION_KEY) private readonly authorizationKey: string,
        @Inject(AUTHORIZATION_METHOD) private readonly authorizationMethod: string) {
    }

    public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        let headers: HttpHeaders = new HttpHeaders();
        if (!!this.session.accessToken) {
            headers = headers.append(
                this.authorizationKey,
                this.authorizationMethod + ' ' + this.session.accessToken);
        }
        return next.handle(request.clone({
            headers
        }));
    }
}
