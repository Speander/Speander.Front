import {
    Inject,
    Injectable,
    OnDestroy
} from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { ACCESS_TOKEN_KEY } from '../injection-tokens/access-token-key';

@Injectable({
    providedIn: 'root'
})
export class SessionService implements OnDestroy {
    public get accessToken(): string | null {
        return this._accessToken;
    }

    private _accessToken: string | null;

    constructor(
        private readonly storage: LocalStorageService,
        @Inject(ACCESS_TOKEN_KEY) private readonly accessTokenKey: string) {
        this._accessToken = null;
    }

    public ngOnDestroy(): void {
    }

    public retrieveToken(): void {
        this._accessToken = this.storage.retrieve(this.accessTokenKey);
    }

    public setToken(token: string): void {
        this._accessToken = token;
        this.storage.store(this.accessTokenKey, token);
    }

    public logout(): void {
        this._accessToken = null;
        this.storage.clear();
    }
}
