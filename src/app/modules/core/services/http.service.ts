import {
    Inject,
    Injectable
} from '@angular/core';
import {
    HttpClient,
    HttpErrorResponse
} from '@angular/common/http';
import {
    Observable,
    throwError
} from 'rxjs';
import { API_URL } from '../injection-tokens/api-url';
import {
    catchError,
    map
} from 'rxjs/operators';
import { IApiResponse } from '../interfaces/api-response';
import { IReadonlyDictionary } from '../interfaces/dictionary';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private readonly http: HttpClient, @Inject(API_URL) private readonly apiUrl: string) { }

    public get<TResponse>(path: string, params?: IReadonlyDictionary<string>): Observable<TResponse> {
        return this.http.get<IApiResponse<TResponse>>(this.apiUrl + path, { params: params }).pipe(
            map((response: IApiResponse<TResponse>) => response.data),
            catchError((error: HttpErrorResponse) => throwError(!!error.error ? error.error.errors : error))
        );
    }

    public post<TRequest, TResponse>(
        path: string,
        body: TRequest,
        params?: IReadonlyDictionary<string>): Observable<TResponse> {
        return this.http.post<IApiResponse<TResponse>>(this.apiUrl + path, body, { params: params }).pipe(
            map((response: IApiResponse<TResponse>) => response.data),
            catchError((error: HttpErrorResponse) => throwError(!!error.error ? error.error.errors : error))
        );
    }

    public put<TRequest, TResponse>(
        path: string,
        body: TRequest,
        params?: IReadonlyDictionary<string>): Observable<TResponse> {
        return this.http.put<IApiResponse<TResponse>>(this.apiUrl + path, body, { params: params }).pipe(
            map((response: IApiResponse<TResponse>) => response.data),
            catchError((error: HttpErrorResponse) => throwError(!!error.error ? error.error.errors : error))
        );
    }

    public patch<TRequest, TResponse>(
        path: string,
        body: TRequest,
        params?: IReadonlyDictionary<string>): Observable<TResponse> {
        return this.http.patch<IApiResponse<TResponse>>(this.apiUrl + path, body, { params: params }).pipe(
            map((response: IApiResponse<TResponse>) => response.data),
            catchError((error: HttpErrorResponse) => throwError(!!error.error ? error.error.errors : error))
        );
    }

    public delete<TResponse>(path: string, params?: IReadonlyDictionary<string>): Observable<TResponse> {
        return this.http.delete<IApiResponse<TResponse>>(this.apiUrl + path, { params: params }).pipe(
            map((response: IApiResponse<TResponse>) => response.data),
            catchError((error: HttpErrorResponse) => throwError(!!error.error ? error.error.errors : error))
        );
    }
}
