import { Forms } from '../enums/forms';
import { LoginForm } from '@speander/domain/forms/login-form';
import { RegisterForm } from '@speander/domain/forms/register-form';

export interface AuthForms {
    [Forms.LOGIN]: LoginForm;
    [Forms.REGISTER]: RegisterForm;
}
