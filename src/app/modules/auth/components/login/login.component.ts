import {
    Component,
    OnDestroy,
    OnInit
} from '@angular/core';
import { AuthService } from '../../services/auth.service';
import {
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { NgFormsManager } from '@ngneat/forms-manager';
import { Forms } from '../../enums/forms';
import { Control } from '@ngneat/forms-manager/lib/types';
import { Mapper } from '@nartc/automapper';
import { LoginRequest } from '@speander/domain/requests/login-request';
import { AuthForms } from '../../interfaces/auth-forms';
import { first } from 'rxjs/operators';
import { LoginForm } from '@speander/domain/forms/login-form';
import { LoginResponse } from '@speander/domain/responses/login-response';
import { SessionService } from '@speander/core/services/session.service';
import { Router } from '@angular/router';

@Component({
    selector: 'sp-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

    public readonly loginForm: FormGroup;

    constructor(
        private readonly auth: AuthService,
        builder: FormBuilder,
        private readonly formsManager: NgFormsManager<AuthForms>,
        private readonly session: SessionService,
        private readonly router: Router) {
        this.loginForm = builder.group({
            email: [
                null,
                Validators.required
            ],
            password: [
                null,
                Validators.required
            ]
        });
        formsManager.upsert(Forms.LOGIN, this.loginForm);
    }

    public ngOnInit(): void {
    }

    public login(): void {
        const form: Control<LoginForm> | null = this.formsManager.getControl(Forms.LOGIN);
        if (!!form && form.valid) {
            this.auth.login(Mapper.map(form.value, LoginRequest, LoginForm)).pipe(first()).subscribe(
                (response: LoginResponse) => {
                    this.session.setToken(response);
                    this.router.navigateByUrl('dashboard');
                }
            );
        }
    }

    public ngOnDestroy(): void {
        this.formsManager.unsubscribe(Forms.LOGIN);
    }
}
