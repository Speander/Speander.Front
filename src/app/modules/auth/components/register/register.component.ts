import {
    Component,
    OnDestroy,
    OnInit
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { NgFormsManager } from '@ngneat/forms-manager';
import { AuthForms } from '../../interfaces/auth-forms';
import { Forms } from '../../enums/forms';
import { Control } from '@ngneat/forms-manager/lib/types';
import { Mapper } from '@nartc/automapper';
import { first } from 'rxjs/operators';
import { equals } from '../../helpers/equals';
import { RegisterRequest } from '@speander/domain/requests/register-request';
import { LoginForm } from '@speander/domain/forms/login-form';
import { RegisterForm } from '@speander/domain/forms/register-form';
import { Router } from '@angular/router';

@Component({
    selector: 'sp-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

    public readonly registerForm: FormGroup;

    constructor(
        private readonly auth: AuthService,
        builder: FormBuilder,
        private readonly formsManager: NgFormsManager<AuthForms>,
        private readonly router: Router) {
        this.registerForm = builder.group(
            {
                email: [
                    null,
                    Validators.required
                ],
                password: [
                    null,
                    Validators.required
                ],
                confirmPassword: [
                    null,
                    Validators.required
                ]
            },
            {
                validator: equals('password', 'confirmPassword')
            });
        formsManager.upsert(Forms.REGISTER, this.registerForm);
    }

    public ngOnInit(): void {
    }

    public register(): void {
        const form: Control<LoginForm> | null = this.formsManager.getControl(Forms.REGISTER);
        if (!!form && form.valid) {
            this.auth.register(Mapper.map(form.value, RegisterRequest, RegisterForm)).pipe(first()).subscribe(
                () => {
                    this.router.navigateByUrl('auth/sign-in');
                }
            );
        }
    }

    public ngOnDestroy(): void {
        this.formsManager.unsubscribe(Forms.REGISTER);
    }
}
