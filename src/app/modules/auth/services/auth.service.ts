import {
    Inject,
    Injectable
} from '@angular/core';
import { HttpService } from '../../core/services/http.service';
import { ENDPOINTS } from '../../core/injection-tokens/endpoints';
import { IReadonlyDictionary } from '../../core/interfaces/dictionary';
import { LoginRequest } from '@speander/domain/requests/login-request';
import { Observable } from 'rxjs';
import { LoginResponse } from '@speander/domain/responses/login-response';
import { Endpoint } from '../../core/enums/endpoint';
import { RegisterRequest } from '@speander/domain/requests/register-request';
import { RegisterResponse } from '@speander/domain/responses/register-response';

@Injectable()
export class AuthService {
    constructor(
        private readonly http: HttpService,
        @Inject(ENDPOINTS) private readonly endpoints: IReadonlyDictionary<string>) {
    }

    public login(loginDto: LoginRequest): Observable<LoginResponse> {
        return this.http.post(this.endpoints[Endpoint.LOGIN], loginDto);
    }

    public register(registerDto: RegisterRequest): Observable<RegisterResponse> {
        return this.http.post(this.endpoints[Endpoint.REGISTER], registerDto);
    }
}
