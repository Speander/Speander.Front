import {
    AbstractControl,
    ValidatorFn
} from '@angular/forms';
import { IReadonlyDictionary } from '../../core/interfaces/dictionary';

/** this control value must be equal to given control's value */
export function equals(targetKey: string, toMatchKey: string): ValidatorFn {
    return (group: AbstractControl): IReadonlyDictionary<any> | null => {
        const target: AbstractControl | null = group.get(targetKey);
        const toMatch: AbstractControl | null = group.get(toMatchKey);
        if (!!target && target.touched && !!toMatch && toMatch.touched) {
            const isMatch = target.value === toMatch.value;
            // set equal value error on dirty controls
            if (!isMatch && target.valid && toMatch.valid) {
                toMatch.setErrors({ equalValue: targetKey });
                const message = targetKey + ' != ' + toMatchKey;
                return { 'equalValue': message };
            }
            if (isMatch && toMatch.hasError('equalValue')) {
                toMatch.setErrors(null);
            }
        }

        return null;
    };
}
