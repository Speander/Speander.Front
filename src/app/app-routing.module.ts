import { NgModule } from '@angular/core';
import {
    RouterModule,
    Routes
} from '@angular/router';
import { NotFoundComponent } from './modules/core/components/not-found/not-found.component';
import { AuthGuard } from '@speander/core/guards/auth.guard';


const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./modules/public/public.module').then(m => m.PublicModule)
    },
    {
        path: 'auth',
        loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'dashboard',
        loadChildren: () => import('./modules/private/modules/dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'events',
        loadChildren: () => import('./modules/private/modules/event/event.module').then(m => m.EventModule),
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
